<?php
session_start();
if (!isset($_GET['lang']))
  $_SESSION['lang'] = NULL;
else
  $_SESSION['lang'] = $_GET['lang']
?>
<?php include 'test.php' ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script>
  $(document).ready(function() {
    var testss = "<?php echo $_GET['lang'] ?>";
    console.log(testss);
  });

  function openForm() {
    $('#myForm').fadeIn();
    $('#myForm').show();
  }

  function closeForm() {
    $('#myForm').fadeOut();
    $('#myForm').show();
  }

  function prayerForm(inputText) {
    var formData = new FormData(document.getElementById("formPrayer"));
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(inputText.value.match(mailformat)){
      axios.post(url + '/api/prayer-request', formData)
        .then(function(response) {
          if (response.data.status == 200)
            alert('Berhasil') ? "" : location.reload();
          else
            alert('Gagal')
        })
    } else {
      alert("You have entered an invalid email address!");
      return false;
    }
  }
</script>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>BICC - Contact Us</title>
  <meta content="" name="Description">
  <meta content="" name="Keyword">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Roboto:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/main.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: UpConstruction - v1.3.0
  * Template URL: https://bootstrapmade.com/upconstruction-bootstrap-construction-website-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header d-flex align-items-center">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="index.html" class="logo d-flex align-items-center">
        <!-- Uncomment the line below if you also wish to use an image logo -->
        <img src="assets/img/hero-carousel/cover/logo.png" alt="">
        <h1>Bandengan Impact City Church</h1>
      </a>

      <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
      <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>
      <nav id="navbar" class="navbar">
        <ul>
          <li><a href="index.php<?php echo $_SESSION['lang'] ?>"><?php echo $home ?></a></li>
          <li><a href="ministry.php<?php echo $_SESSION['lang'] ?>"><?php echo $ministry ?></a></li>
          <li><a href="ourservices.php<?php echo $_SESSION['lang'] ?>"><?php echo $services ?></a></li>
          <li><a style="color:#FFD700;text-decoration:bold" href="contact.php<?php echo $_SESSION['lang'] ?>"><?php echo $contact ?></a></li>
          <li style="margin-right: 30px;"><a href="about.php<?php echo $_SESSION['lang'] ?>"><?php echo $about ?></a></li>
          <select name="menu1" class="form-select" aria-label="Default select example" onChange="MM_jumpMenu('parent',this,0)">
            <option value="?lang=id" <?php echo ($_GET['lang'] == 'id') ? 'selected' : '' ?>>Indonesia</option>
            <option value="?lang=en" <?php echo (!isset($_GET['lang']) || $_GET['lang'] == 'en') ? 'selected' : '' ?>>English</option>
          </select>
        </ul>
      </nav>

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs d-flex align-items-center" style="background-image: url('assets/img/breadcrumbs-bg.jpg');">
      <div class="container position-relative d-flex flex-column align-items-center" data-aos="fade">

        <h2><?php echo $contact; ?></h2>
        <ol>
          <li><a href="index.php"><?php echo $home; ?></a></li>
          <li><?php echo $contact; ?></li>
        </ol>
        <ol>
          <li><a href="#contact"><?php echo $ourAddress; ?></a></li>
          <li><a href="#get-started"><?php echo $prayerRequests; ?></a></li>
        </ol>

      </div>
    </div><!-- End Breadcrumbs -->

    <!-- ======= Contact Section ======= -->

    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="row gy-3">
          <div class="col-lg-3">
            <div class="info-item d-flex flex-column justify-content-center align-items-center">
              <i class="bi bi-map"></i>
              <h3><?php echo $ourAddress; ?></h3>
              <p align="center">Bandengan Selatan No. 41B RT 06 RW 09 Jakarta Barat 11240</p>
            </div>
          </div><!-- End Info Item -->

          <div class="col-lg-3 col-md-6">
            <div class="info-item d-flex flex-column justify-content-center align-items-center">
              <i class="bi bi-envelope"></i>
              <h3><?php echo $email; ?></h3>
              <p>contact@bicc.or.id<br>&#20</p>
            </div>
          </div><!-- End Info Item -->

          <div class="col-lg-3 col-md-6">
            <div class="info-item  d-flex flex-column justify-content-center align-items-center">
              <i class="bi bi-telephone"></i>
              <h3><?php echo $call; ?></h3>
              <p>(021) 6913046<br>&#20</p>
            </div>
          </div><!-- End Info Item -->

          <div class="col-lg-3 col-md-6">
            <div class="info-item  d-flex flex-column justify-content-center align-items-center">
              <i class="bi bi-whatsapp"></i>
              <h3><?php echo $chat; ?></h3>
              <p>0858-8285-0007<br>&#20</p>
            </div>
          </div><!-- End Info Item -->

        </div>

        <div class="row gy-4 mt-1">
          <div class="col-lg-15 ">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.955428807754!2d106.79899191430941!3d-6.136690961870869!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f61dfc2300fb%3A0x33fc967aabbf53b!2sGSJA%20BICC!5e0!3m2!1sid!2sid!4v1678718446739!5m2!1sid!2sid" frameborder="0" style="border:0; width: 100%; height: 384px;" allowfullscreen></iframe>
          </div><!-- End Google Maps -->
        </div>
      </div>
    </section>

    <section id="get-started" class="get-started section-bg">
      <div class="container">

        <div class="row justify-content-between gy-4">

          <div class="col-lg-6 d-flex align-items-center" data-aos="fade-up">
            <div class="content">
              <h3><?php echo $prayerRequests; ?></h3>
              <p>Doa merupakan hak istimewa yang diberikan Tuhan bagi orang percaya untuk berkomunikasi, berbicara, bersekutu dengan-Nya.</p>
              <p>Saat kita menundukkan kepala, merendahkan diri, berserah, kita menyatu dalam hadirat-Nya, kita membawa segala apa yang kita rasakan dan inginkan ke dalam kuasa-Nya.</p>
              <p>ia akan selalu mendengar dan memahami doa-doa kita. Dan apabila Dia berkenan, Dia akan menjawab segala keluhan dan permohonan yang kita sampaikan, sesuai kehendak-Nya. Perlu dipahami bahwa apa yang Tuhan berikan ialah rancangan yang baik adanya bagi kita. Itulah kekuatan doa</p>
              <p>Markus 11:24 - Karena itu Aku berkata kepadamu: apa saja yang kamu minta dan doakan, percayalah bahwa kamu telah menerimanya, maka hal itu akan diberikan kepadamu.</p>
            </div>
          </div>

          <div class="col-lg-5" data-aos="fade">
            <form class="php-email-form" id="formPrayer" name="formPrayer">
              <h3><?php echo $formPrayerRequests; ?></h3>
              <p>Apakah saudara sedang mengalami persoalan, pergumulan, membutuhkan pertolongan, dan rindu untuk didoakan? Tim kami siap untuk mendoakan saudara.</p>
              <div class="row gy-3">

                <div class="col-md-12">
                  <input type="text" name="nama" class="form-control" placeholder="<?php echo $name; ?>" required>
                </div>

                <div class="col-md-12 ">
                  <input type="email" class="form-control" name="email" placeholder="Email" required>
                </div>

                <div class="col-md-12">
                  <input type="text" class="form-control" name="phone" placeholder="<?php echo $phone; ?>" onkeypress="return isNumberKey(event)" required>
                </div>

                <div class="col-md-12">
                  <textarea class="form-control" name="prayer" rows="6" placeholder="<?php echo $prayer; ?>" required></textarea>
                </div>

                <div class="col-md-12 text-center">
                  <div class="loading">Loading</div>
                  <div class="error-message"></div>
                  <div class="sent-message">Your prayer request has been sent successfully. Thank you!</div>

                  <button type="button" class="btn btn-warning" onclick="prayerForm(document.formPrayer.email)"><?php echo $send; ?></button>
                </div>

              </div>
            </form>
          </div><!-- End Quote Form -->

        </div>

      </div>
    </section>
  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="footer-content position-relative">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6">
            <div class="footer-info">
              <h3>Bandengan Impact City Church</h3>
              <p>
                Bandengan Selatan No. 41B RT 06 RW 09 Pekojan <br>
                Jakarta Barat, 11240<br><br>
                <strong><?php echo $phone; ?>:</strong> (021) 6913046<br>
                <strong>Email:</strong> contact@bicc.or.id<br>
              </p>
              <div class="social-links d-flex mt-3">
                <a href="https://www.facebook.com/gsjabandenganselatan?mibextid=LQQJ4d" class="d-flex align-items-center justify-content-center"><i class="bi bi-facebook"></i></a>
                <a href="https://instagram.com/gsja_bicc?igshid=NTdlMDg3MTY=" class="d-flex align-items-center justify-content-center"><i class="bi bi-instagram"></i></a>
                <a href="https://www.youtube.com/channel/UCV9foQOsrzpVwBRLVQ6Xr_w" class="d-flex align-items-center justify-content-center"><i class="bi bi-youtube"></i></a>
                <a href="https://wa.me/6285882850007" class="d-flex align-items-center justify-content-center"><i class="bi bi-whatsapp"></i></a>
              </div>
            </div>
          </div><!-- End footer info column-->
          <div class="col-lg-2 col-md-3 footer-links">
            <h4><?php echo $connectedWithUs; ?></h4>
            <ul>
              <li><a href="index.php<?php echo $_SESSION['lang'] ?>"><?php echo $home ?></a></li>
              <li><a href="ministry.php<?php echo $_SESSION['lang'] ?>"><?php echo $ministry ?></a></li>
              <li><a href="ourservices.php<?php echo $_SESSION['lang'] ?>"><?php echo $services ?></a></li>
              <li style="margin-right: 30px;"><a href="contact.php<?php echo $_SESSION['lang'] ?>"><?php echo $contact ?></a></li>
              <li><a href="about.php<?php echo $_SESSION['lang'] ?>"><?php echo $about ?></a></li>
            </ul>
          </div><!-- End footer links column-->
        </div>
      </div>

      <div class="footer-legal text-center position-relative">
        <div class="container">
          <div class="copyright">
            &copy; Copyright <strong><span>Bandengan Impact City Church</span></strong>. All Rights Reserved
          </div>
          <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/upconstruction-bootstrap-construction-website-template/ -->
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
          </div>
        </div>
      </div>
  </footer>
  <!-- End Footer -->

  <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <div class="btn-message" onclick="openForm()">
    <span class="material-icons mt-2">contact_support
    </span>
  </div>

  <?php include("chat-popup.php") ?>

  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

  <script>
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && charCode != 46 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
  </script>
</body>

</html>