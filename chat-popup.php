<?php
$sesLang = $_SESSION['lang'];
$url = 'https://api-bicc.beatfraps.com';
$qna = json_decode(file_get_contents($url . '/api/qna'));
?>
<div class="chat-popup border border-secondary rounded" id="myForm">
    <form action="/action_page.php" class="form-container">
        <button onclick="clearChat()" style="float: right;">Akhiri Chat</button>
        <h3>Chat</h3>

        <label for="msg"><b>Message</b></label>
        <div class="border border-secondary rounded" style="overflow-x: scroll;overflow-x: hidden;max-height:300px;flex-direction: column-reverse;display: flex;">
            <!-- <div class="container lighter border border-secondary rounded">
                <h6>nama</h6>
                <p>Hello. How are you today?</p>
                <span class="time-right">11:00</span>
            </div>

            <div class="container darker">
                <h6>test</h6>
                <p>Hey! I'm fine. Thanks for asking!</p>
                <span class="time-left">11:01</span>
            </div> -->
            <div id="allChats"></div>
        </div>
        <div id="inputMessage" style="display: none">
            <textarea placeholder="Type message.." id="chat" name="chat" required></textarea>

            <button type="button" class="btn" onclick="sendChat()">Send</button>
        </div>
        <div id="chatbot" style="height: 200px;overflow-x: scroll;">
            <?php foreach ($qna as $q) { ?>
                <button type="button" class="btn" onclick="chatBot(<?php echo $q->id ?>)"><?php echo $q->question ?></button>
            <?php } ?>
            <button type="button" class="btn" onclick="openMessage()">Live Chat</button>
        </div>
        <button type="button" class="btn cancel" onclick="closeForm()">Close</button>
    </form>
</div>

<div class="chat-popup" id="regisForm">
    <h3>Masukkan Nama</h3>
    <form class="form-container">
        <textarea placeholder="Nama" id="newUser" name="newUser" required></textarea>
        <textarea placeholder="Email" id="newEmail" name="newEmail" required></textarea>

        <button type="button" class="btn" onclick="RegisForm()">Send</button>
        <button type="button" class="btn cancel" onclick="closeRegisForm()">Close</button>
    </form>
</div>

<script>
    var newUser;

    function openForm() {
        newUser = Cookies.get('name')
        if (newUser == null)
            document.getElementById("regisForm").style.display = "block";
        else {
            document.getElementById("myForm").style.display = "block";
            getAllChats();
            setInterval(function() {
                getAllChats();
            }, 2000);
        }
    }

    function closeForm() {
        document.getElementById("myForm").style.display = "none";
    }

    function getTime(val) {
        var val_date = new Date(val);
        var temp = val_date.toString().split(" ");
        return temp[4].slice(0, 5);
    }

    function RegisForm() {
        var newRegis = document.getElementById("newUser").value;
        var newMail = document.getElementById("newEmail").value;
        var formData = new FormData();
        formData.append('nama', newRegis);
        formData.append('email', newMail);
        axios({
            method: 'post',
            url: url + '/api/chat-user',
            data: formData,
        }).then(function(response) {
            var now = new Date();
            var time = now.getTime();
            var expireTime = time + 1000 * 3600 * 24;
            now.setTime(expireTime);
            Cookies.set('name', response.data, {
                expires: now
            });
            document.getElementById("regisForm").style.display = "none";
            document.getElementById("myForm").style.display = "block";
            newUser = Cookies.get('name');
            getAllChats();
            setInterval(function() {
                getAllChats();
            }, 2000);
        });
    }

    function closeRegisForm() {
        document.getElementById("regisForm").style.display = "none";
    }

    function sendChat() {
        var chat = document.getElementById("chat").value;
        var formData = new FormData();
        if (mess != '')
            formData.append('chat', mess)
        else
            formData.append('chat', chat)
        axios({
            method: 'post',
            url: url + '/api/chat/' + newUser,
            data: formData,
        }).then(function(response) {
            document.getElementById("chat").value = '';
            mess = '';
        });
    }

    function chatBot(val) {
        axios.get(url + '/api/qna/' + val)
            .then(function(response) {
                mess = response.data.question;
                sendChat();
            })
    }

    function getAllChats() {
        axios({
            method: 'get',
            url: url + '/api/chat/' + newUser
        }).then(function(response) {
            var data = response.data.allChats;
            var allChats = '';
            data.forEach(function(item, index) {
                var temp = '';
                if (item.user_id == response.data.user.id) {
                    temp += '<div class="container darker"><h6>' + response.data.user.nama;
                } else {
                    temp += '<div class="container lighter border border-secondary rounded"><h6>Admin';
                }
                temp += '</h6><p>' + item.chat + '</p><span class="time-left">' + getTime(item.created_at) + '</span></div>';

                allChats += temp;
            })
            document.getElementById('allChats').innerHTML = allChats;
        })
    }

    function openMessage() {
        document.getElementById("chatbot").style.display = "none";
        document.getElementById("inputMessage").style.display = "block";
    }

    function clearChat() {
        Cookies.remove('name');
        document.getElementById("regisForm").style.display = "block";
        document.getElementById("myForm").style.display = "none";
    }
</script>