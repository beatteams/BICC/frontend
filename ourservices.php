<?php
session_start();
if (!isset($_GET['lang']))
  $_SESSION['lang'] = NULL;
else
  $_SESSION['lang'] = $_GET['lang'];
$url = 'https://api-bicc.beatfraps.com';
$activity = json_decode(file_get_contents($url . '/api/activity'));
?>
<?php include 'test.php' ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script>
  $(document).ready(function() {
    var testss = "<?php echo $_GET['lang'] ?>";
    console.log(testss);
  });

  function openForm() {
    $('#myForm').fadeIn();
    $('#myForm').show();
  }

  function closeForm() {
    $('#myForm').fadeOut();
    $('#myForm').show();
  }
</script>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>BICC - Our Services</title>
  <meta content="" name="Description">
  <meta content="" name="Keyword">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Roboto:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/main.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: UpConstruction - v1.3.0
  * Template URL: https://bootstrapmade.com/upconstruction-bootstrap-construction-website-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header d-flex align-items-center">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="index.php" class="logo d-flex align-items-center">
        <img src="assets/img/hero-carousel/cover/logo.png" alt="">
        <h1>Bandengan Impact City Church</h1>
      </a>

      <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
      <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>
      <nav id="navbar" class="navbar">
        <ul>
          <li><a href="index.php<?php echo $_SESSION['lang'] ?>"><?php echo $home ?></a></li>
          <li><a href="ministry.php<?php echo $_SESSION['lang'] ?>"><?php echo $ministry ?></a></li>
          <li><a style="color:#FFD700;text-decoration:bold" href="ourservices.php<?php echo $_SESSION['lang'] ?>"><?php echo $services ?></a></li>
          <li style="margin-right: 30px;"><a href="contact.php<?php echo $_SESSION['lang'] ?>"><?php echo $contact ?></a></li>
          <li style="margin-right: 30px;"><a href="about.php<?php echo $_SESSION['lang'] ?>"><?php echo $about ?></a></li>
          <select name="menu1" class="form-select" aria-label="Default select example" onChange="MM_jumpMenu('parent',this,0)">
            <option value="?lang=id" <?php echo ($_GET['lang'] == 'id') ? 'selected' : '' ?>>Indonesia</option>
            <option value="?lang=en" <?php echo (!isset($_GET['lang']) || $_GET['lang'] == 'en') ? 'selected' : '' ?>>English</option>
          </select>
        </ul>
      </nav>

    </div>
  </header><!-- End Header -->

  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <section id="hero" class="hero">

      <div id="hero-carousel" class="carousel slide" data-bs-ride="carousel" data-bs-interval="5000">
        <div class="carousel-item active" style="background-image: url(assets/img/hero-carousel/cover/jadwal.jpg); background-size: 100% 100%"></div>
        <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/cover/agape.jpg); background-size: 100% 100%"></div>
        <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/cover/menado.jpg); background-size: 100% 100%"></div>
        <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/cover/intimacy.jpg); background-size: 100% 100%"></div>
        <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/cover/sosmed.jpg); background-size: 100% 100%"></div>
        <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/cover/hikmat.jpg); background-size: 100% 100%"></div>

        <a class="carousel-control-prev" href="#hero-carousel" role="button" data-bs-slide="prev">
          <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
        </a>

        <a class="carousel-control-next" href="#hero-carousel" role="button" data-bs-slide="next">
          <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
        </a>

      </div>

      <div class="col-lg-7col-lg-5 d-flex flex-column justify-content-center">
        <h3>
          <p style="text-align: center; margin-top: 30px;"><b> Ibadah Raya </b></p>
        </h3>
        <p style="text-align: center;">Ibadah Raya I : Pukul 06.30 WIB</p>
        <p style="text-align: center;">Ibadah Raya II : Pukul 08.45 WIB</p>
        <p style="text-align: center;">Ibadah Raya III : Pukul 11.00 WIB</p>
        <p style="text-align: center;">Ibadah Teens : Pukul 11.00 WIB</p>
        <p style="text-align: center;">Ibadah Youth : Pukul 13.15 WIB</p>
        <p style="text-align: center;">Pancaran Berkat : Pukul 09.00 WIB</p>
      </div>

      <div class="col-lg-7col-lg-5 d-flex flex-column justify-content-center">
        <h3>
          <p style="text-align: center; margin-top: 30px;"><b> Ibadah Sekolah Minggu </b></p>
        </h3>
        <p style="text-align: center;">Sekolah Minggu I : Pukul 08.00 WIB</p>
        <p style="text-align: center;">Sekolah Minggu II : Pukul 11.00 WIB</p>
      </div>

      <div class="col-lg-7col-lg-5 d-flex flex-column justify-content-center">
        <h3>
          <p style="text-align: center; margin-top: 30px;"><b> Jadwal Menara Doa </b></p>
        </h3>
        <p style="text-align: center;">Senin - Jumat : Pukul 18.50 WIB</p>
      </div>
      <div class="col-lg-7col-lg-5 d-flex flex-column justify-content-center">
        <h3>
          <p style="text-align: center; margin-top: 30px;"><b> Jadwal Morning With Jesus </b></p>
        </h3>
        <p style="text-align: center;">Sabtu : Pukul 06.10 WIB</p>
      </div>
    </section>
    </div>
    </div>
    </section>

    <section id="constructions" class="constructions">
      <div class="container" data-aos="fade-up">

        <div class="section-header">
          <h2><?php echo $GalleryActivity; ?></h2>
          <p>Berikut adalah galeri activity BICC yang telah sukses dilaksanakan.</p>
        </div>

        <div class="row gy-4">
          <?php foreach ($activity as $a) { ?>
            <div class="col-lg-6" data-aos="fade-up" data-aos-delay="100">
              <div class="card-item">
                <div class="row">
                  <div class="col-xl-5">
                    <div class="card-bg" style="background-image: url(<?php echo $url ?>/api/image/<?php echo $a->gambar ?>);"></div>
                  </div>
                  <div class="col-xl-7 d-flex align-items-center">
                    <div class="card-body">
                      <h4 class="card-title"><?php echo $a->judul ?></h4>
                      <p><?php echo $a->deskripsi ?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- End Card Item -->
          <?php } ?>
        </div>

      </div>
    </section><!-- End Constructions Section -->
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="footer-content position-relative">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6">
            <div class="footer-info">
              <h3>Bandengan Impact City Church</h3>
              <p>
                Bandengan Selatan No. 41B RT 06 RW 09 Pekojan<br>
                Jakarta Barat, 11240<br><br>
                <strong><?php echo $phone; ?>:</strong> (021) 6913046<br>
                <strong>Email:</strong> contact@bicc.or.id<br>
              </p>
              <div class="social-links d-flex mt-3">
                <a href="https://www.facebook.com/gsjabandenganselatan?mibextid=LQQJ4d" class="d-flex align-items-center justify-content-center"><i class="bi bi-facebook"></i></a>
                <a href="https://instagram.com/gsja_bicc?igshid=NTdlMDg3MTY=" class="d-flex align-items-center justify-content-center"><i class="bi bi-instagram"></i></a>
                <a href="https://www.youtube.com/channel/UCV9foQOsrzpVwBRLVQ6Xr_w" class="d-flex align-items-center justify-content-center"><i class="bi bi-youtube"></i></a>
                <a href="https://wa.me/6285882850007" class="d-flex align-items-center justify-content-center"><i class="bi bi-whatsapp"></i></a>
              </div>
            </div>
          </div><!-- End footer info column-->
          <div class="col-lg-2 col-md-3 footer-links">
            <h4><?php echo $connectedWithUs; ?></h4>
            <ul>
              <li><a href="index.php<?php echo $_SESSION['lang'] ?>"><?php echo $home ?></a></li>
              <li><a href="ministry.php<?php echo $_SESSION['lang'] ?>"><?php echo $ministry ?></a></li>
              <li><a href="ourservices.php<?php echo $_SESSION['lang'] ?>"><?php echo $services ?></a></li>
              <li style="margin-right: 30px;"><a href="contact.php<?php echo $_SESSION['lang'] ?>"><?php echo $contact ?></a></li>
              <li><a href="about.php<?php echo $_SESSION['lang'] ?>"><?php echo $about ?></a></li>
            </ul>
          </div><!-- End footer links column-->
        </div>
      </div>

      <div class="footer-legal text-center position-relative">
        <div class="container">
          <div class="copyright">
            &copy; Copyright <strong><span>Bandengan Impact City Church</span></strong>. All Rights Reserved
          </div>
          <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/upconstruction-bootstrap-construction-website-template/ -->
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
          </div>
        </div>
      </div>
  </footer>
  <!-- End Footer -->

  <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <div class="btn-message" onclick="openForm()">
    <span class="material-icons mt-2">contact_support
    </span>
  </div>

  <?php include("chat-popup.php") ?>

  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>