<?php
session_start();
if (!isset($_GET['lang']))
  $_SESSION['lang'] = NULL;
else
  $_SESSION['lang'] = $_GET['lang']
?>
<?php include 'test.php' ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script>
  $(document).ready(function() {
    var testss = "<?php echo $_GET['lang'] ?>";
    console.log(testss);
  });

  function openForm() {
    $('#myForm').fadeIn();
    $('#myForm').show();
  }

  function closeForm() {
    $('#myForm').fadeOut();
    $('#myForm').show();
  }
</script>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>BICC - About</title>
  <meta content="" name="Description">
  <meta content="" name="Keyword">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Roboto:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/main.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: UpConstruction - v1.3.0
  * Template URL: https://bootstrapmade.com/upconstruction-bootstrap-construction-website-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header d-flex align-items-center">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="index.html" class="logo d-flex align-items-center">
        <img src="assets/img/hero-carousel/cover/logo.png" alt="">
        <h1>Bandengan Impact City Church</h1>
      </a>

      <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
      <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>
      <nav id="navbar" class="navbar">
        <ul>
          <li><a href="index.php<?php echo $_SESSION['lang'] ?>"><?php echo $home ?></a></li>
          <li><a href="ministry.php<?php echo $_SESSION['lang'] ?>"><?php echo $ministry ?></a></li>
          <li><a href="ourservices.php<?php echo $_SESSION['lang'] ?>"><?php echo $services ?></a></li>
          <li><a href="contact.php<?php echo $_SESSION['lang'] ?>"><?php echo $contact ?></a></li>
          <li style="margin-right: 30px;"><a style="color:#FFD700;text-decoration:bold" href="about.php<?php echo $_SESSION['lang'] ?>"><?php echo $about ?></a></li>
          <select name="menu1" class="form-select" aria-label="Default select example" onChange="MM_jumpMenu('parent',this,0)">
            <option value="?lang=id" <?php echo ($_GET['lang'] == 'id') ? 'selected' : '' ?>>Indonesia</option>
            <option value="?lang=en" <?php echo (!isset($_GET['lang']) || $_GET['lang'] == 'en') ? 'selected' : '' ?>>English</option>
          </select>
          <!-- <ul>
          <li><a href="index.html">Home</a></li>
          <li><a href="about.html" class="active">About</a></li>
          <li><a href="services.html">Services</a></li>
          <li><a href="projects.html">Projects</a></li>
          <li><a href="blog.html">Blog</a></li>
          <li class="dropdown"><a href="#"><span>Dropdown</span> <i class="bi bi-chevron-down dropdown-indicator"></i></a>
            <ul>
              <li><a href="#">Dropdown 1</a></li>
              <li class="dropdown"><a href="#"><span>Deep Dropdown</span> <i class="bi bi-chevron-down dropdown-indicator"></i></a>
                <ul>
                  <li><a href="#">Deep Dropdown 1</a></li>
                  <li><a href="#">Deep Dropdown 2</a></li>
                  <li><a href="#">Deep Dropdown 3</a></li>
                  <li><a href="#">Deep Dropdown 4</a></li>
                  <li><a href="#">Deep Dropdown 5</a></li>
                </ul>
              </li>
              <li><a href="#">Dropdown 2</a></li>
              <li><a href="#">Dropdown 3</a></li>
              <li><a href="#">Dropdown 4</a></li>
            </ul>
          </li>
          <li><a href="contact.html">Contact</a></li> -->
        </ul>
      </nav>

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs d-flex align-items-center" style="background-image: url('assets/img/breadcrumbs-bg.jpg');">
      <div class="container position-relative d-flex flex-column align-items-center" data-aos="fade">

        <h2><?php echo $about; ?> BICC</h2>
        <ol>
          <li><a href="index.php"><?php echo $home; ?></a></li>
          <li><?php echo $about; ?></li>
        </ol>
        <ol>
          <li><a href="#about"><?php echo $history; ?></a></li>
          <li><a href="#alt-services"><?php echo $vision . ' & ' . $mission; ?></a></li>
          <li><a href="#team"><?php echo $ourLeaders; ?></a></li>
          <li><a href="#contact"><?php echo $ourAddress; ?></a></li>
        </ol>

      </div>
    </div><!-- End Breadcrumbs -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">
        <div class="row position-relative">
          <div class="col-lg-7 about-img">
            <section id="hero" class="hero">
              <div id="hero-carousel" class="carousel slide" data-bs-ride="carousel" data-bs-interval="5000">
                <div class="carousel-item active" style="background-image: url(assets/img/hero-carousel/sejarah1.jpg)"></div>
                <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/sejarah2.jpg)"></div>
                <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/sejarah3.jpg)"></div>
                <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/sejarah4.jpg)"></div>
                <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/sejarah5.jpg)"></div>
                <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/sejarah6.jpg)"></div>

                <a class="carousel-control-prev" href="#hero-carousel" role="button" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
                </a>
                <a class="carousel-control-next" href="#hero-carousel" role="button" data-bs-slide="next">
                  <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
                </a>
              </div>
            </section>
          </div>

          <div class="col-lg-7">
            <h2><?php echo $history; ?> BICC</h2>
            <div class="our-story">
              <h4><?php echo $establishedSince; ?> 1963</h4>
              <h3><?php echo $ourStory; ?></h3>
              <p>oleh karena kebesaran Tuhan, GSJA BICC lahir pada tanggal 11 September 1964.</p>
              <p>Dibawah kepemimpinan dan Senior Pastor (Alm Pdt Jonatan Irwan Pribadi) untuk pertama kalinya di Jalan Gang Jembatan Hitam GSJA BICC.</p>
              <p>11 September 1964 menjadi sebuah hari yang tidak bisa dilupakan bagi perjalanan GSJA BICC dibawah kepemimpinan dari Alm Pdt Jonatan Irwan Pribadi karena tepat di hari itu untuk pertama kalinya Ibadah umum pun akhirnya diadakan dan resmilah lahir Sebuah Gereja dengan nama GSJA Jembatan Hitam.</p>
              <p>Dengan bangungan yang hanya berukuran 3x4 meter dan dimulai dari beberapa jemaat saja  bahkan sarat dengan kesederhanaan ini merupakan cikal bakal dari GSJA BICC yang saat ini memiliki ribuan jemaat.</p>
              <p>29 Tahun perjalanan GSJA Jembatan Hitam dari Jemaat yang hanya sedikit, ruangan yang kecil dan panas serta lokasi gereja yang berada di sebuah gang sempit, Tuhan pakai Gereja ini menjadi tempat penuaian jiwa-jiwa.</p>
              <P>Melihat lokasi Gereja yang sudah tidak mencukupi dengan kapasitas jemaat yang hadir setiap minggunya, maka di tahun 1993 karena Anugerah dan oleh Mujizat dari Tuhan Yesus Kristus, GSJA BICC memperluas  rumah ibadah yang baru yang berada di sebuah jalan besar dengan bangunan yang luas dan ruang ibadah yang bisa menampung ratusan bahkan ribuan jemaat lengkap dengan lift dan Full AC.</P>
              <p>31 Januari 2021 dengan visi yang baru ditanamkan oleh Tuhan untuk menjadi gereja masa depan yang berdampak kedalam maupun keluar, GSJA Bandengan kemudian berganti nama menjadi GSJA BICC kemudian di teruskan penggembalaannya oleh anak dari Alm Pdt Jonatan Irwan Pribadi yaitu Pdt Daniel Pribadi.</p>
              <ul>
              </ul>
            </div>
            <div class="watch-video d-flex align-items-center position-relative">
                <i class="bi bi-play-circle"></i>
                <a href="https://www.youtube.com/watch?v=2zX8MQCc7P8" class="glightbox stretched-link">Watch Video</a>
              </div>
          </div>
        </div>
    </section>
    <!-- End About Section -->

    </div>

    </div>
    </section><!-- End Stats Counter Section -->

    <!-- ======= Alt Services Section ======= -->
    <section id="alt-services" class="alt-services">
      <div class="container" data-aos="fade-up">

        <div class="row justify-content-around gy-4">
          <div class="col-lg-6 img-bg" style="background-image: url(assets/img/hero-carousel/sejarah6.jpg);" data-aos="zoom-in" data-aos-delay="100"></div>

          <div class="col-lg-5 d-flex flex-column justify-content-center">
            <h3><?php echo $vision . ' & ' . $mission; ?></h3>
            <h3><?php echo $vision; ?></h3>
            <p>Menjadi gereja masa depan yang kuat yang melahirkan generasi bintang yang bersinar dan berdampak ke dalam maupun ke luar.</p>
            <p> </p>
            <h3><?php echo $mission; ?></h3>
            <div class="icon-box d-flex position-relative" data-aos="fade-up" data-aos-delay="100">
              <i style="width: 170px;" class="bi bi-buildings-fill"></i>
              <div>
                <h4>Membangun Dasar Mutlak Gereja</h4>
                <p>Membangun dasar-dasar mutlak gereja berdasarkan hukum-hukum Tuhan</p>
              </div>
            </div><!-- End Icon Box -->

            <div class="icon-box d-flex position-relative" data-aos="fade-up" data-aos-delay="200">
              <i style="width: 230px;" height: 70px;" class="bi bi-star-fill"></i>
              <div>
                <h4>SPORTIVE</h4>
                <p>Melahirkan generasi masa depan (next generation) yang SPORTIVE (Strong, Positive, Active, dan Creative)</p>
              </div>
            </div><!-- End Icon Box -->

            <div class="icon-box d-flex position-relative" data-aos="fade-up" data-aos-delay="300">
              <i style="width: 315px;" height: 70px;" class="bi bi-heart-fill"></i>
              <div>
                <h4>Generasi Cinta Tuhan</a></h4>
                <p>Melahirkan para Penjaga Pintu Gerbang (PPG) dan jemaat yang cinta Tuhan lewat ibadah, menara doa, baca Alkitab dan homecell atau komunitas</p>
              </div>
            </div><!-- End Icon Box -->

            <div class="icon-box d-flex position-relative" data-aos="fade-up" data-aos-delay="400">
              <i style="width: 140px;" class="bi bi-stack"></i>
              <div>
                <h4>Sistem Yang Terpadu</a></h4>
                <p>Membangun sistem informasi teknologi yang kuat dan terpadu (IT)</p>
              </div>
            </div><!-- End Icon Box -->

            <div class="icon-box d-flex position-relative" data-aos="fade-up" data-aos-delay="400">
              <i class="bi bi-people-fill" style="width: 100px;"></i>
              <div>
                <h4>Radikal</a></h4>
                <p>Membangun kegerakan misi yang radikal</p>
              </div>
            </div><!-- End Icon Box -->

          </div>
        </div>

      </div>
    </section><!-- End Alt Services Section -->

    <!-- ======= Our Team Section ======= -->
    <section id="team" class="team">
      <div class="container" data-aos="fade-up">

        <div class="section-header">
          <h2><?php echo $ourLeaders; ?></h2>
          <p>BICC dipimpin oleh hamba-hamba Tuhan yang memiliki kerinduan untuk melayani Tuhan.</p>
        </div>

        <div class="row gy-5">

          <div class="col-lg-4 col-md-6 member" data-aos="fade-up" data-aos-delay="100">
            <div class="member-img">
              <img src="assets/img/team/Jonathan.jpeg" class="img-fluid" alt="">
            </div>
            <div class="member-info text-center">
              <h4>Alm. Pdt. Jonathan Pribadi</h4>
              <span>Pendiri Gereja BICC</span>
            </div>
          </div><!-- End Team Member -->

          <div class="col-lg-4 col-md-6 member" data-aos="fade-up" data-aos-delay="200">
            <div class="member-img">
              <img src="assets/img/team/danielp.jpg" class="img-fluid" alt="">
            </div>
            <div class="member-info text-center">
              <h4>Pdt. Daniel Pribadi</h4>
              <span>Gembala Sidang BICC</span>
            </div>
          </div><!-- End Team Member -->

          <div class="col-lg-4 col-md-6 member" data-aos="fade-up" data-aos-delay="300">
            <div class="member-img">
              <img src="assets/img/team/daniels.jpeg" class="img-fluid" alt="">
            </div>
            <div class="member-info text-center">
              <h4>Pdt. Daniel Setiawan</h4>
              <span>Pemimpin Ibadah Raya 1</span>
            </div>
          </div><!-- End Team Member -->

          <div class="col-lg-4 col-md-6 member" data-aos="fade-up" data-aos-delay="400">
            <div class="member-img">
              <img src="assets/img/team/stepen.jpg" class="img-fluid" alt="">
            </div>
            <div class="member-info text-center">
              <h4>Pdt. Stephen Pribadi</h4>
              <span>Pemimpin Ibadah Raya 2</span>
            </div>
          </div><!-- End Team Member -->

          <div class="col-lg-4 col-md-6 member" data-aos="fade-up" data-aos-delay="500">
            <div class="member-img">
              <img src="assets/img/team/cecilia.jpg" class="img-fluid" alt="">
            </div>
            <div class="member-info text-center">
              <h4>Pdt. Cecilia Pribadi</h4>
              <span>Pemimpin Ibadah Raya 3</span>
            </div>
          </div><!-- End Team Member -->

          <div class="col-lg-4 col-md-6 member" data-aos="fade-up" data-aos-delay="600">
            <div class="member-img">
              <img src="assets/img/team/priska.jpg" class="img-fluid" alt="">
            </div>
            <div class="member-info text-center">
              <h4>Pdt. Priska Sujiati</h4>
              <span>Pemimpin Ibadah Raya Teens</span>
            </div>
          </div><!-- End Team Member -->
          <div class="col-lg-4 col-md-6 member" data-aos="fade-up" data-aos-delay="400">
            <div class="member-img">
              <img src="assets/img/team/hartono.jpg" class="img-fluid" alt="">
            </div>
            <div class="member-info text-center">
              <h4>Pdt. Hartono</h4>
              <span>Pendeta</span>
            </div>
          </div><!-- End Team Member -->

          <div class="col-lg-4 col-md-6 member" data-aos="fade-up" data-aos-delay="500">
            <div class="member-img">
              <img src="assets/img/team/agie.jpg" class="img-fluid" alt="">
            </div>
            <div class="member-info text-center">
              <h4>Pdt. Deddy Rahardjo</h4>
              <span>Pendeta</span>
            </div>
          </div><!-- End Team Member -->

          <div class="col-lg-4 col-md-6 member" data-aos="fade-up" data-aos-delay="600">
            <div class="member-img">
              <img src="assets/img/team/lidya.jpg" class="img-fluid" alt="">
            </div>
            <div class="member-info text-center">
              <h4>Pdt. Lidya Pribadi</h4>
              <span>Pemimpin Ibadah Sekolah Minggu</span>
            </div>
          </div><!-- End Team Member -->
        </div>

      </div>
    </section><!-- End Our Team Section -->

  <!-- ======= Contact Section ======= -->
  <section id="contact" class="contact">
      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="row gy-3">
          <div class="col-lg-3">
            <div class="info-item d-flex flex-column justify-content-center align-items-center">
              <i class="bi bi-map"></i>
              <h3><?php echo $ourAddress; ?></h3>
              <p align="center">Bandengan Selatan No. 41B RT 06 RW 09 Jakarta Barat 11240</p>
            </div>
          </div><!-- End Info Item -->

          <div class="col-lg-3 col-md-6">
            <div class="info-item d-flex flex-column justify-content-center align-items-center">
              <i class="bi bi-envelope"></i>
              <h3><?php echo $email; ?></h3>
              <p>contact@bicc.or.id<br>&#20</p>
            </div>
          </div><!-- End Info Item -->

          <div class="col-lg-3 col-md-6">
            <div class="info-item  d-flex flex-column justify-content-center align-items-center">
              <i class="bi bi-telephone"></i>
              <h3><?php echo $call; ?></h3>
              <p>(021) 6913046<br>&#20</p>
            </div>
          </div><!-- End Info Item -->

          <div class="col-lg-3 col-md-6">
            <div class="info-item  d-flex flex-column justify-content-center align-items-center">
              <i class="bi bi-whatsapp"></i>
              <h3><?php echo $chat; ?></h3>
              <p>0858-8285-0007<br>&#20</p>
            </div>
          </div><!-- End Info Item -->

      </div>

      <div class="row gy-4 mt-1">
        <div class="col-lg-15 ">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.955428807754!2d106.79899191430941!3d-6.136690961870869!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f61dfc2300fb%3A0x33fc967aabbf53b!2sGSJA%20BICC!5e0!3m2!1sid!2sid!4v1678718446739!5m2!1sid!2sid" frameborder="0" style="border:0; width: 100%; height: 384px;" allowfullscreen></iframe>
        </div><!-- End Google Maps -->
      </div>
    </div>
  </section>

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">

    <div class="footer-content position-relative">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6">
            <div class="footer-info">
              <h3>Bandengan Impact City Church</h3>
              <p>
                Bandengan Selatan No. 41B RT 06 RW 09 Pekojan <br>
                Jakarta Barat, 11240<br><br>
                <strong><?php echo $phone; ?>:</strong> (021) 6913046<br>
                <strong>Email:</strong> contact@bicc.or.id<br>
              </p>
              <div class="social-links d-flex mt-3">
                <a href="https://www.facebook.com/gsjabandenganselatan?mibextid=LQQJ4d" class="d-flex align-items-center justify-content-center"><i class="bi bi-facebook"></i></a>
                <a href="https://instagram.com/gsja_bicc?igshid=NTdlMDg3MTY=" class="d-flex align-items-center justify-content-center"><i class="bi bi-instagram"></i></a>
                <a href="https://www.youtube.com/channel/UCV9foQOsrzpVwBRLVQ6Xr_w" class="d-flex align-items-center justify-content-center"><i class="bi bi-youtube"></i></a>
                <a href="https://wa.me/6285882850007" class="d-flex align-items-center justify-content-center"><i class="bi bi-whatsapp"></i></a>
              </div>
            </div>
          </div><!-- End footer info column-->

          <div class="col-lg-2 col-md-3 footer-links">
            <h4><?php echo $connectedWithUs; ?></h4>
            <ul>
              <li><a href="index.php<?php echo $_SESSION['lang'] ?>"><?php echo $home ?></a></li>
              <li><a href="ministry.php<?php echo $_SESSION['lang'] ?>"><?php echo $ministry ?></a></li>
              <li><a href="ourservices.php<?php echo $_SESSION['lang'] ?>"><?php echo $services ?></a></li>
              <li style="margin-right: 30px;"><a href="contact.php<?php echo $_SESSION['lang'] ?>"><?php echo $contact ?></a></li>
              <li><a href="about.php<?php echo $_SESSION['lang'] ?>"><?php echo $about ?></a></li>
            </ul>
          </div><!-- End footer links column-->
          </div>
        </div>
      </div>

      <div class="footer-legal text-center position-relative">
        <div class="container">
          <div class="copyright">
            &copy; Copyright <strong><span>Bandengan Impact City Church</span></strong>. All Rights Reserved
          </div>
          <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/upconstruction-bootstrap-construction-website-template/ -->
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
          </div>
        </div>
      </div>

  </footer>
  <!-- End Footer -->

  <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <div class="btn-message" onclick="openForm()">
    <span class="material-icons mt-2">contact_support
    </span>
  </div>

  <?php include("chat-popup.php"); ?>

  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>