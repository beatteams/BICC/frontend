<?php
session_start();
if (!isset($_GET['lang']))
  $_SESSION['lang'] = NULL;
else
  $_SESSION['lang'] = $_GET['lang']
?>
<?php
include 'test.php';
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script>
  $(document).ready(function() {
    var testss = "<?php echo $_GET['lang'] ?>";
    console.log(testss);
  });

  function openForm() {
    $('#myForm').fadeIn();
    $('#myForm').show();
  }

  function closeForm() {
    $('#myForm').fadeOut();
    $('#myForm').show();
  }
</script>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>BICC Website</title>
  <meta content="" name="Description">
  <meta content="" name="keyword">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Roboto:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/main.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header d-flex align-items-center">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="index.php" class="logo d-flex align-items-center">
        <img src="assets/img/hero-carousel/cover/logo.png" alt="">
        <h1>Bandengan Impact City Church</h1>
      </a>

      <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
      <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>
      <nav id="navbar" class="navbar">
        <ul>
          <li><a style="color:#FFD700;text-decoration:bold" href="index.php<?php echo $_SESSION['lang'] ?>"><?php echo $home ?></a></li>
          <li><a href="ministry.php<?php echo $_SESSION['lang'] ?>"><?php echo $ministry ?></a></li>
          <li><a href="ourservices.php<?php echo $_SESSION['lang'] ?>"><?php echo $services ?></a></li>
          <li><a href="contact.php<?php echo $_SESSION['lang'] ?>" active><?php echo $contact ?></a></li>
          <li style="margin-right: 30px;"><a href="about.php<?php echo $_SESSION['lang'] ?>"><?php echo $about ?></a></li>
          <select name="menu1" class="form-select" aria-label="Default select example" onChange="MM_jumpMenu('parent',this,0)">
            <option value="?lang=id" <?php echo ($_GET['lang'] == 'id') ? 'selected' : '' ?>>Indonesia</option>
            <option value="?lang=en" <?php echo (!isset($_GET['lang']) || $_GET['lang'] == 'en') ? 'selected' : '' ?>>English</option>
          </select>
        </ul>
      </nav>

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="hero">

    <div class="info d-flex align-items-center">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6 text-center">
            <b>
              <h1 data-aos="fade-down" color="white">Bandengan Impact<span> City Church</span></h1>
            </b>
            <p data-aos="fade-up">GSJA Bandengan atau yang sekarang dikenal sebagai Bandengan Impact City Church (BICC) adalah sebuah gereja profetik. Sejak berdirinya gereja ini tahun 1963, gereja ini semakin dipenuhi oleh kasih karunia Allah Roh Kudus sehingga dapat berkembang hingga saat ini. BICC saat ini telah berkembang menjadi gereja yang baik dan memiliki visi menjangkau 5000 jiwa.</p>
          </div>
        </div>
      </div>
    </div>

    <div class="welcoming-video" bis_skin_checked="1"><video style="width: 100%; margin-bottom: 25px; margin-top: 0px" autoplay="" muted="" loop="" src="assets/img/hero-carousel/cover/welcoming.mp4"></video></div>

    <div id="hero-carousel" class="carousel slide" data-bs-ride="carousel" data-bs-interval="5000">
      <div class="carousel-item active" style="background-image: url(assets/img/hero-carousel/cover/jadwal.jpg); background-size: 100% 100%"></div>
      <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/cover/agape.jpg); background-size: 100% 100%"></div>
      <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/cover/menado.jpg); background-size: 100% 100%"></div>
      <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/cover/intimacy.jpg); background-size: 100% 100%"></div>
      <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/cover/sosmed.jpg); background-size: 100% 100%"></div>
      <div class="carousel-item" style="background-image: url(assets/img/hero-carousel/cover/hikmat.jpg); background-size: 100% 100%"></div>

      <a class="carousel-control-prev" href="#hero-carousel" role="button" data-bs-slide="prev">
        <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
      </a>

      <a class="carousel-control-next" href="#hero-carousel" role="button" data-bs-slide="next">
        <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
      </a>

    </div>

  </section><!-- End Hero Section -->

  <main id="" main>

    <section id="constructions" class="constructions">
      <div class="container" data-aos="fade-up">

        <div class="section-header">
          <h2><?php echo $pastor; ?></h2>
          <p></p>
        </div>

        <center>
          <div class="image">
            <image style="width: 50%; align: center;" image-align="center" justify-content="center" src="assets/img/hero-carousel/koden.JPG">
              </iamge>
          </div>
        </center>
        <div class="col-lg-7col-lg-5 d-flex flex-column justify-content-center">
          <h3>
            <p style="text-align: center;"><b> Pdt. Daniel Pribadi & Family </b></p>
          </h3>
          <p style="text-align: center;">Pdt. Daniel Pribadi adalah Anak Kedua dari Pdt. Jonathan Pribadi dan sebagai Gembala Sidang dari Bandengan Impact City Church. Melayani sebagai pengkhotbah baik di gereja lokal, gereja dalam kota, dan gereja luar kota. Pdt. Daniel Pribadi juga menjabat sebagai Kepala Kaum Muda DKMN. Menikah dengan Ps. Jennie, pernikahan mereka dikaruniai tiga orang anak yang cinta Tuhan.</p>
        </div>
      </div>

      </div>
    </section><!-- End Constructions Section -->
    <!-- ======= Updated News Section ======= -->
    <section id="recent-blog-posts" class="recent-blog-posts">
      <div class="container" data-aos="fade-up"">

    
    
  <div class=" section-header">
        <h2><?php echo $updatedNews; ?></h2>
        <p></p>
      </div>

      <div class="row gy-5" id="allNews">


      </div>

      </div>
    </section>
    <!-- End Recent Blog Posts Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="footer-content position-relative">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6">
            <div class="footer-info">
              <h3>Bandengan Impact City Church</h3>
              <p>
                Bandengan Selatan No. 41B RT 06 RW 09 Pekojan <br>
                Jakarta Barat, 11240<br><br>
                <strong><?php echo $phone; ?>:</strong> (021) 6913046<br>
                <strong>Email:</strong> contact@bicc.or.id<br>
              </p>
              <div class="social-links d-flex mt-3">
                <a href="https://www.facebook.com/gsjabandenganselatan?mibextid=LQQJ4d" class="d-flex align-items-center justify-content-center"><i class="bi bi-facebook"></i></a>
                <a href="https://instagram.com/gsja_bicc?igshid=NTdlMDg3MTY=" class="d-flex align-items-center justify-content-center"><i class="bi bi-instagram"></i></a>
                <a href="https://www.youtube.com/channel/UCV9foQOsrzpVwBRLVQ6Xr_w" class="d-flex align-items-center justify-content-center"><i class="bi bi-youtube"></i></a>
                <a href="https://wa.me/6285882850007" class="d-flex align-items-center justify-content-center"><i class="bi bi-whatsapp"></i></a>
              </div>
            </div>
          </div><!-- End footer info column-->
          <div class="col-lg-2 col-md-3 footer-links">
            <h4><?php echo $connectedWithUs; ?></h4>
            <ul>
              <li><a href="index.php<?php echo $_SESSION['lang'] ?>"><?php echo $home ?></a></li>
              <li><a href="ministry.php<?php echo $_SESSION['lang'] ?>"><?php echo $ministry ?></a></li>
              <li><a href="ourservices.php<?php echo $_SESSION['lang'] ?>"><?php echo $services ?></a></li>
              <li style="margin-right: 30px;"><a href="contact.php<?php echo $_SESSION['lang'] ?>"><?php echo $contact ?></a></li>
              <li><a href="about.php<?php echo $_SESSION['lang'] ?>"><?php echo $about ?></a></li>
            </ul>
          </div><!-- End footer links column-->
        </div>
      </div>

      <div class="footer-legal text-center position-relative">
        <div class="container">
          <div class="copyright">
            &copy; Copyright <strong><span>Bandengan Impact City Church</span></strong>. All Rights Reserved
          </div>
          <div class="credits">
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
          </div>
        </div>
      </div>

    </div>
    </div>
    </div>
  </footer>
  <!-- End Footer -->

  <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <div class="btn-message" onclick="openForm()">
    <span class="material-icons mt-2">contact_support
    </span>
  </div>

  <?php include("chat-popup.php") ?>

  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

  <script>
    $(document).ready(function() {
      var sesVal = "<?php echo $sesLang ?>";
      axios.get(url + '/api/news')
        .then(function(response) {
          var temp = response.data;
          var allNews = '';
          temp.forEach(element => {
            allNews += news(element, 'page-1.php?id=' + element.id, sesVal);
          });

          document.getElementById("allNews").innerHTML = allNews;
        })
    });
  </script>

</body>

</html>